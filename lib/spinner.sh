# example:
# spin "make  >> out 2>&1"
#

spinner() {
	local state=1
	local proc=$1
	echo -n ' '
	while [ -d /proc/$proc ];do
		case $state in
			1) echo -n '/'; state=2;;
			2) echo -n '-'; state=3;;
			3) echo -n '\'; state=4;;
			4) echo -n '|'; state=1;;
		esac
		sleep 0.1
		echo -en '\b'
	done
	echo -en ' \b'
	return 0
}

spin() {
	(eval "$@") &
	pid=$!
	(spinner $pid) &
	spinner_pid=$!
	wait $spinner_pid $pid
	return $?
}

