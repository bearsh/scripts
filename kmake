#!/bin/bash
# (c) 2014, me@bearsh.org
# This program is free software: you can redistribute it and/or modify
# it under the terms of the 3 clause BSD license

source "/lib/gentoo/functions.sh"
shopt -s extglob

ret=''

localversion=${LOCALVERSION}
kbuild_dir=${KBUILD_OUTPUT}
: ${kbuild_dir:=$(portageq envvar KBUILD_OUTPUT 2> /dev/null)}
: ${kbuild_dir:="/var/cache/build_kernel"}
make_j="$(portageq envvar MAKEOPTS | sed -r 's/.*(-j[0-9]+).*/\1/')"
: ${make_j:="-j3"}

print_help() {
	echo "kmake OPTION ACTION"
	echo "ACTION: enter, boot, grub, dracut, kernel make targets"
	echo " -h  Print this help"
	echo " -k  KBUILD_OUTPUT, default '/var/cache/build_kernel'"
	echo " -l  Localversion to append"
	echo ""
	echo "Actions:"
	echo "  enter"
	echo "    Enter /usr/src/linux and optionally setup symlink to"
	echo "    specified kernel version"
	echo "  boot"
	echo "    synonym for 'dracut grub'"
	echo "  dracut"
	echo "    if dracuts exists, call it"
	echo "  grub"
	echo "    call grub's mkconfig"
	echo "  merge"
	echo "    synonym for 'install modules_install'"
	echo "  all other actions are passed as targets to the kernel build system"
}

latest_kernel_linked() {
	[[ $(eselect kernel list | grep -n "\*" | cut -d : -f 1) == $(eselect kernel list | wc -l) ]]
}

myself=$(readlink -f "$0")

env='KBUILD_OUTPUT=${kbuild_dir} '

args=()
while [[ $# > 0 ]]; do
	key="${1}"
	shift
	case ${key} in
		-l)
			[[ $# < 1 ]] && { print_help; exit 1; }
			localversion="$1"
			env+='LOCALVERSION=${localversion} '
			shift
			;;
		-k)
			[[ $# < 1 ]] && { print_help; exit 1; }
			kbuild_dir="$1"
			shift
			;;
		-h|--help)
			print_help
			exit 1
			;;
		*)
			args+=(${key})
			;;
	esac
done

env=$(eval echo "${env}")
#echo "env: ${env}"
export ${env}

for arg in "${args[@]}"; do
	case $arg in
		*install*)
			#echo sudo KBUILD_OUTPUT=${kbuild_dir} make -j3 ${args[@]}
			sudo ${env} make ${make_j} ${arg}
			ret=$?
			;;
		enter)
			kdir="/usr/src/linux"
			if ! latest_kernel_linked; then
				einfo "'/usr/src/linux' doens't point to latest installed version"
				einfo "eselect kernel list"
				eselect kernel list
				read -p "press 'c' to continue, 'q' to quit or type a number to enter, if prefixed with 's', also set up symlink: " choice
				#echo $choice
				case ${choice} in
					c)
						;;
					q)
						exit 1
						;;
					+([0-9]))
						kdir="$(eselect kernel list | grep "\[${choice}\]" 2>/dev/null | awk '{print $2}')"
						[[ -z ${kdir} ]] && { eerror "could not determine kernel version/directory, exiting..."; exit 1; }
						kdir="/usr/src/${kdir}"
						;;
					s+([0-9]))
						einfo "calling eselect to set symlink"
						sudo eselect kernel set ${choice#s} || exit 1
						;;
					*)
						eerror "didn't understand, exiting..."
						exit 1
						;;
				esac
			fi
			#cd ${kdir} && env PS1="[kmake] ${PS1}" bash --norc -i || exit 1
			cd ${kdir} && bash || exit 1
			exit 0
			;;
		boot)
			${myself} dracut grub
			ret=$?
			;;
		grub)
			sudo grub-mkconfig -o /boot/grub/grub.cfg
			ret=$?
			;;
		dracut)
			if [[ -z $(which dracut 2>/dev/null) ]]; then
				einfo "didn't find dracut in path, skipping"
				ret=0
			else
				path="$(realpath .)"
				kver="${path#*-}"
				read -p "calling dracut with following kernel version: '${kver}${localversion}' (y/n)? " choice
				if [[ $choice =~ ^[YyJj]$ ]]; then
					sudo dracut --force '' ${kver}${localversion}
					ret=$?
				else
					einfo "call \"dracut --force '' KERNEL_VERSION\" as root"
					ret=0
				fi
			fi
			;;
		merge)
			${myself} install modules_install
			ret=$?
			;;
	esac
	# exit on error immediately
	[[ -n "$ret" && "$ret"  != 0 ]] && exit $ret
done

[ -n "$ret" ] && exit $ret

make ${make_j} ${args[@]}
