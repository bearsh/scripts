#!/bin/sh
# taken from https://gist.github.com/Taverius/1f7f2d61d8469dec2ce9

DATE_TO_PRUNE='-60 day'

DB_CURRENT="quassel-storage.sqlite"
DB_BACKUP="$DB_CURRENT.bak"
DB_TEMP="$DB_CURRENT.tmp"

die() {
    echo $@
    exit 1
}

#make backup and working copy
echo -n "Creating backup .."

mv "$DB_CURRENT" "$DB_BACKUP" || die "Cannot create backup"
cp "$DB_BACKUP" "$DB_TEMP" || die "Cannot create temp file"
echo ". done!"

#purge backlog
echo -n "Purging backlog .."

sqlite3 $DB_TEMP "DELETE FROM backlog WHERE time < strftime('%s','now','${DATE_TO_PRUNE}');" || die "Purge failed"
echo ". done!"

#mainteinance
echo -n "Vacuuming .."
sqlite3 $DB_TEMP "VACUUM" || die "Vacuum failed"

echo ". done!"

echo -n "Reindexing .."
sqlite3 $DB_TEMP "REINDEX" || die "Reindex failed"

echo ". done!"

#copy
echo -n "Copying .."
cp "$DB_TEMP" "$DB_CURRENT" || die "Copy failed"

echo ". done!"

#clean up
echo -n "Cleaning up .."
rm "$DB_TEMP" || die "Cleaning up failed"

echo ". done!"
