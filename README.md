# bearsh' scripts #

### enchrome ###
wrapper around google-chrome


```
#!bash

enchrome [OPTIONS] [URL]
  -a,--app            start url in app mode
  -i,--incognito      incognito mode
  -t,--tmpdir DIR    use DIR as tmp dir
  -n,--no-tor         don't use tor proxy
  -d,--do-not-clean   don't clean up tmp dir afterwards

```

### pic2pdf ###
embeds a jpg or png in a pdf using latex


```
#!bash

pic2pdf .jpg|.png [.pdf]
```

### kmake ###
help building kernel and friends as user

```
#!bash

kmake OPTION ACTION
ACTION: enter, boot, grub, dracut, kernel make targets
 -h  Print this help
 -k  KBUILD_OUTPUT, default '/var/cache/build_kernel'
 -l  Localversion to append
```